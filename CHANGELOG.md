
## 0.0.14 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/forward-networks-create-intent-checks!7

---

## 0.0.13 [05-22-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/forward-networks-create-intent-checks!6

---

## 0.0.12 [01-06-2022]

* Certify on 2021.2

See merge request itentialopensource/pre-built-automations/forward-networks-create-intent-checks!5

---

## 0.0.11 [11-22-2021]

* Update pre-built description

See merge request itentialopensource/pre-built-automations/forward-networks-create-intent-checks!4

---

## 0.0.10 [07-01-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/forward-networks-create-intent-checks!3

---

## 0.0.9 [05-04-2021]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/forward-networks-create-intent-checks!1

---

## 0.0.8 [05-03-2021]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/forward-networks-create-intent-checks!1

---

## 0.0.7 [05-03-2021]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/forward-networks-create-intent-checks!1

---

## 0.0.6 [05-03-2021]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/forward-networks-create-intent-checks!1

---

## 0.0.5 [05-03-2021]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/forward-networks-create-intent-checks!1

---

## 0.0.4 [05-03-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.3 [05-03-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.2 [05-03-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n
